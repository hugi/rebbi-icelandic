package is.rebbi.icelandic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.StoredFields;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.TopScoreDocCollectorManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http://en.wikipedia.org/wiki/Icelandic_language
 */

public class IcelandicInflector {

	private static Logger logger = LoggerFactory.getLogger( IcelandicInflector.class );

	/*
	 * Fields stored in the index.
	 */
	private static final String F_TYPE = "type";
	private static final String F_WORD = "word";
	private static final String F_CATEGORY = "category";
	private static final String F_MODIFIED_WORD = "modifiedWord";
	private static final String F_MODIFIED_WORD_LENGTH = "modifiedWordLength";
	private static final String F_MODIFIERS = "modifiers";

	/*
	 * Lazily initialized variables (documented in corresponding accessor methods).
	 */
	private Path _indexPath;
	private Directory _indexDirectory;
	private IndexSearcher _indexSearcher;

	/**
	 * Constructs an instance using the specified file locations.
	 *
	 * @param indexPath      The location of the index on disk.
	 */
	public IcelandicInflector( final String indexPathString ) {
		Objects.requireNonNull( indexPathString, "You must provide path to an index folder when constructing " + IcelandicInflector.class.getSimpleName() );
		_indexPath = Path.of( indexPathString );

		if( !Files.exists( _indexPath ) ) {
			logger.info( "Note that no index exist at the specified location '{}'. You should start by creating an index using createIndex()" );
		}
	}

	/**
	 * @return The location of the index folder on disk.
	 */
	private Path indexPath() {
		return _indexPath;
	}

	/**
	 * @return The index used to store the files.
	 * @throws IOException If the index location is invalid.
	 */
	private Directory indexDirectory() throws IOException {
		if( _indexDirectory == null ) {
			_indexDirectory = FSDirectory.open( indexPath() );
		}

		return _indexDirectory;
	}

	/**
	 * @return The index used to store the files.
	 * @throws IOException If the index location is invalid.
	 */
	private IndexSearcher indexSearcher() throws IOException {
		if( _indexSearcher == null ) {
			IndexReader reader = DirectoryReader.open( indexDirectory() );
			_indexSearcher = new IndexSearcher( reader );
		}

		return _indexSearcher;
	}

	/**
	 * Generates the index. If an index already exists, it will be deleted and a new one created in it's stead.
	 */
	public void createIndex( InputStream stream ) {
		logger.info( "Starting index creation" );

		try( BufferedReader sourceFileReader = new BufferedReader( new InputStreamReader( stream ) ) ;) {
			IndexWriterConfig config = new IndexWriterConfig( analyzer() );
			config.setOpenMode( OpenMode.CREATE );
			IndexWriter writer = new IndexWriter( indexDirectory(), config );

			int currentLineIndex = 0;
			String line = null;

			while( (line = sourceFileReader.readLine()) != null ) {
				String[] record = line.split( ";" );
				addWordDefinition( writer, record[0], record[2], record[3], record[4], record[5] );
				if( ++currentLineIndex % 100000 == 0 ) {
					logger.info( "Reading line {}: {},{},{},{},{}", new Object[] { String.valueOf( currentLineIndex ), record[0], record[2], record[3], record[4], record[5] } );
				}
			}

			logger.info( "Finished reading words, almost done. Please wait for a few seconds while I finish up..." );
			writer.close();
			logger.info( "Finished creating index. A total of " + currentLineIndex + " word forms were indexed. IcelandicInflector is now ready for use." );
		}
		catch( Exception e ) {
			logger.error( "An error occurred while creating the index", e );
		}
	}

	/**
	 * Adds a single word definition to the index.
	 */
	private static void addWordDefinition( IndexWriter indexWriter, String word, String type, String category, String modifiedWord, String modifiers ) throws CorruptIndexException, IOException {
		Document doc = new Document();
		doc.add( field( F_WORD, word ) );
		doc.add( field( F_TYPE, type ) );
		doc.add( field( F_CATEGORY, category ) );
		doc.add( field( F_MODIFIED_WORD, modifiedWord ) );
		doc.add( field( F_MODIFIED_WORD_LENGTH, String.valueOf( modifiedWord.length() ) ) );
		doc.add( field( F_MODIFIERS, modifiers ) );
		indexWriter.addDocument( doc );
	}

	private static Field field( String name, String value ) {
		return new TextField( name, value, Field.Store.YES );
	}

	/**
	 * @return A phrase where each word is modified using the given modifier string. Capitalization is maintained.
	 */
	public String phrase( String phrase, String modifierString ) {
		String[] words = phrase.split( " " );

		StringBuilder b = new StringBuilder();

		for( String word : words ) {
			String modifiedWord = word( word, modifierString );

			if( Util.isCapitalized( word ) ) {
				modifiedWord = Util.capitalize( modifiedWord );
			}

			b.append( modifiedWord );
			b.append( " " );
		}

		return b.toString();
	}

	public boolean exists( String word ) {
		try {
			String queryString = F_MODIFIED_WORD + ":" + word;
			Query query = new QueryParser( F_MODIFIED_WORD, analyzer() ).parse( queryString );
			ScoreDoc[] hits = indexSearcher().search( query, 1000 ).scoreDocs;
			return hits.length > 0;
		}
		catch( ParseException e ) {
			throw new RuntimeException( "An error occurred while parsing your query", e );
		}
		catch( IOException e ) {
			throw new RuntimeException( "An IO error occurred while checking if a word exists", e );
		}
	}

	/**
	 * @return Every possible form of a given word.
	 *
	 * @param word The word to search for, nominative singular
	 * @param type Type of word (optional)
	 */
	public List<String> formsOfWord( String word, String type ) {
		List<String> results = new ArrayList<>();

		try {
			IndexSearcher searcher = indexSearcher();
			String queryString = F_WORD + ":" + word;

			if( type != null ) {
				queryString += " AND " + F_TYPE + ":" + type;
			}

			final Query query = new QueryParser( F_WORD, analyzer() ).parse( queryString );
			final ScoreDoc[] hits = searcher.search( query, 1000 ).scoreDocs;

			final Set<String> modifiedWords = new HashSet<>();

			final StoredFields storedFields = searcher.getIndexReader().storedFields();

			for( int i = 0; i < hits.length; ++i ) {
				final Document doc = storedFields.document( hits[i].doc );
				final String modifiedWord = doc.get( F_MODIFIED_WORD );
				modifiedWords.add( modifiedWord );
			}

			results = new ArrayList<>( modifiedWords );
		}
		catch( Exception e ) {
			logger.error( "Failed to find forms of word: " + word, e );
		}

		Collections.sort( results );
		return results;
	}

	/**
	 * @return Every possibly form of a given word.
	 *
	 * @param word The word to search for, nominative singular
	 * @param type Type of word (optional)
	 */
	public List<String> formsOfWordSearchingEveryForm( String word, String type ) {

		List<String> results = new ArrayList<>();
		IndexSearcher searcher = null;

		try {
			String queryString = F_MODIFIED_WORD + ":" + word;

			if( type != null ) {
				queryString += " AND " + F_TYPE + ":" + type;
			}

			Query query = new QueryParser( F_MODIFIED_WORD, analyzer() ).parse( queryString );
			searcher = indexSearcher();
			ScoreDoc[] hits = searcher.search( query, 1000 ).scoreDocs;
			Set<String> modifiedWords = new HashSet<>();

			final StoredFields storedFields = searcher.getIndexReader().storedFields();

			for( int i = 0; i < hits.length; ++i ) {
				final Document doc = storedFields.document( hits[i].doc );
				String fetchedWord = doc.get( F_WORD );
				modifiedWords.addAll( formsOfWord( fetchedWord, null ) );
			}

			results = new ArrayList<>( modifiedWords );
		}
		catch( Exception e ) {
			logger.error( "Failed to find forms of word: " + word, e );
		}

		Collections.sort( results );
		return results;
	}

	/**
	 * @return The modified form of a word. Null if no matching word is found .
	 */
	public String word( String word, String modifierString ) {

		String result = null;

		try {
			IndexSearcher searcher = indexSearcher();
			String queryString = F_WORD + ":" + word + " AND " + F_MODIFIERS + ":" + modifierString;
			Query q = new QueryParser( F_MODIFIED_WORD, analyzer() ).parse( queryString );
			final TopScoreDocCollectorManager manager = new TopScoreDocCollectorManager( 1, 1 );
			TopScoreDocCollector collector = manager.newCollector();
			searcher.search( q, collector );
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			final StoredFields storedFields = searcher.getIndexReader().storedFields();

			for( int i = 0; i < hits.length; ++i ) {
				final Document doc = storedFields.document( hits[i].doc );
				String modifiedWord = doc.get( F_MODIFIED_WORD );
				result = modifiedWord;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		return result;
	}

	public String gender( String word ) {

		String result = null;

		try {
			IndexSearcher searcher = indexSearcher();
			String queryString = F_WORD + ":" + word;
			Query q = new QueryParser( F_MODIFIED_WORD, analyzer() ).parse( queryString );
			TopScoreDocCollector collector = new TopScoreDocCollectorManager( 1, 1 ).newCollector();
			searcher.search( q, collector );
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			final StoredFields storedFields = searcher.getIndexReader().storedFields();

			for( int i = 0; i < hits.length; ++i ) {
				final Document doc = storedFields.document( hits[i].doc );
				String modifiedWord = doc.get( F_TYPE );
				result = modifiedWord;
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * @return The modified form of a word. Null if no matching word is found .
	 */
	public List<String> matching( String word, Integer maxLength, int maxMatches, boolean unique ) {

		Collection<String> results = null;

		if( unique ) {
			results = new HashSet<>();
		}
		else {
			results = new ArrayList<>();
		}

		try {
			final IndexSearcher searcher = indexSearcher();
			String queryString = F_MODIFIED_WORD + ":" + word;

			if( maxLength != null ) {
				queryString += " AND " + F_MODIFIED_WORD_LENGTH + ":\"" + maxLength + "\"";
			}

			final QueryParser parser = new QueryParser( F_MODIFIED_WORD, analyzer() );
			parser.setAllowLeadingWildcard( true );

			final Query query = parser.parse( queryString );

			final TopScoreDocCollectorManager collectorManager = new TopScoreDocCollectorManager( maxMatches, maxMatches );
			final TopDocs topDocs = searcher.search( query, collectorManager );
			final ScoreDoc[] hits = topDocs.scoreDocs;

			final StoredFields storedFields = searcher.getIndexReader().storedFields();

			for( int i = 0; i < hits.length; ++i ) {
				final Document doc = storedFields.document( hits[i].doc );
				String modifiedWord = doc.get( F_MODIFIED_WORD );
				results.add( modifiedWord );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		if( results instanceof Set ) {
			results = new ArrayList<>( results );
		}

		Collections.sort( (List<String>)results );

		return (List<String>)results;
	}

	private static final Analyzer analyzer() {
		return new LowercaseAnalyzer();
	}

	private static class LowercaseAnalyzer extends Analyzer {

		@Override
		protected TokenStreamComponents createComponents( final String fieldName ) {
			final Tokenizer src = new WhitespaceTokenizer();
			TokenStream tok = new LowerCaseFilter( src );
			return new TokenStreamComponents( src, tok );
		}
	}

	/**
	 * An inner class containing some utility methods. Methods here should't really be part of this class file but are included nonetheless to minimize dependencies.
	 */
	private static class Util {

		/**
		 * Indicates if the given word is capitalized. Generic utility method.
		 *
		 * @return true only if a string starting with an upper case letter.
		 */
		private static boolean isCapitalized( String string ) {

			if( string == null || string.length() == 0 ) {
				return false;
			}

			String original = string.substring( 0, 1 );
			String converted = original.toUpperCase();
			return original.equals( converted );
		}

		/**
		 * Capitalizes the given string.
		 *
		 * @return The given string, with the first letter of the first word in upper case.The rest of the string is left unmodified.
		 */
		private static String capitalize( String s ) {

			if( s == null || s.length() == 0 ) {
				return s;
			}

			return s.substring( 0, 1 ).toUpperCase() + s.substring( 1 );
		}
	}
}