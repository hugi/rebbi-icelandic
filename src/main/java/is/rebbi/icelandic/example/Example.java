package is.rebbi.icelandic.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import is.rebbi.icelandic.IcelandicInflector;

/**
 * Shows the most common use cases for our inflector.
 */

public class Example {

	public static void main( String[] args ) {
		IcelandicInflector inflector = new IcelandicInflector( "/Users/hugi/tmp/IcelandicInflector/index" );

		try {
			inflector.createIndex( new FileInputStream( new File( "/Users/hugi/tmp/IcelandicInflector/SHsnid.csv" ) ) );
		}
		catch( FileNotFoundException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		inflector.formsOfWord( "hestur", null );
		System.out.println( inflector.phrase( "hestur", "þfft" ) );
	}
}