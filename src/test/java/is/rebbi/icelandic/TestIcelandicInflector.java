package is.rebbi.icelandic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import is.rebbi.icelandic.IcelandicInflector;

public class TestIcelandicInflector {

	private static final String TEST_DATA_FILENAME = "/SHsnid_subset.csv";

	@Test
	public void word() {
		File testIndexFile = null;

		try {
			testIndexFile = File.createTempFile( UUID.randomUUID().toString(), "" );
			testIndexFile.delete();
		}
		catch( IOException e ) {
			e.printStackTrace();
		}

		IcelandicInflector inflector = new IcelandicInflector( testIndexFile.getAbsolutePath() );
		inflector.createIndex( getClass().getResourceAsStream( TEST_DATA_FILENAME ) );

		final List<String> expected = new ArrayList<>();
		expected.add( "aðstoðarhest" );
		expected.add( "aðstoðarhesta" );
		expected.add( "aðstoðarhestana" );
		expected.add( "aðstoðarhestanna" );
		expected.add( "aðstoðarhestar" );
		expected.add( "aðstoðarhestarnir" );
		expected.add( "aðstoðarhesti" );
		expected.add( "aðstoðarhestinn" );
		expected.add( "aðstoðarhestinum" );
		expected.add( "aðstoðarhests" );
		expected.add( "aðstoðarhestsins" );
		expected.add( "aðstoðarhestum" );
		expected.add( "aðstoðarhestunum" );
		expected.add( "aðstoðarhestur" );
		expected.add( "aðstoðarhesturinn" );

		assertEquals( expected, inflector.formsOfWord( "aðstoðarhestur", null ) );
	}
}